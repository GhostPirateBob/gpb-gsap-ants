const elem = document.getElementById("dude");
const countElem = document.getElementById("count");

window.antId = 1;
window.toggle = 1;

setInterval(() => {
  window.toggle = !window.toggle;
  elem.style.transform = `scaleX(${window.toggle ? "-1" : "1"})`;
  countElem.innerText = `${window.antId} × ants`;
}, (Math.random() * 2000));

gsap.registerPlugin(MotionPathPlugin);

document.body.onload = function() {
  window.tl = gsap.timeline();
  window.tlB = gsap.timeline();
  window.tlC = gsap.timeline();
  const duration = 18;
  const items = 36;
  const delay = 1;
  function antDefaults(path) {
    return {
      motionPath: {
        path: path,
        align: path,
        alignOrigin: [Math.random(), Math.random()],
        autoRotate: false
      },
      duration,
      ease: "Power0.easeNone",
      repeat: -1,
      yoyo: false
    };
  }

  for (let i = 1; i < items; ++i) {
    createAntElem(`ant-${i}`);
    ++window.antId;
    createAntElem(`antB-${i}`);
    ++window.antId;
    createAntElem(`antC-${i}`);
    ++window.antId;
    console.log(`creating "#ant-${i}"`);
  }
  for (let i = 1; i < items; ++i) {
    console.log(`running tl.to("#ant-${i}")`);
    // const staggerTiming = `+=${(delay * i)}`;
    const staggerTiming = "+=0.25";
    window.tl.to(`#ant-${i}`, 1, antDefaults("#path"), staggerTiming);
    window.tlB.to(`#antB-${i}`, 1, antDefaults("#pathB"), staggerTiming);
    window.tlC.to(`#antC-${i}`, 1, antDefaults("#pathC"), staggerTiming);
  }
  GSDevTools.create();
};

function createAntElem(id) {
  const animWrapperElem = document.getElementById("anim");
  const img = new Image();
  img.src = "/img/ant.svg";
  img.id = id;
  img.className = "c-anim__ant";
  animWrapperElem.appendChild(img);
}
